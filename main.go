package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	DB_HOST     = "34.128.118.25"
	DB_PORT     = 5432
	DB_USER     = "postgres"
	DB_PASSWORD = "123123"
	DB_NAME     = "coach"
)

type Programs struct {
	ID         int
	Name       string
	Created_on string
}

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal("FAILED: failed to open a connection to the DB", err.Error())
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal("FAILED: failed to ping the DB", err.Error())
	}

	fmt.Println("Successfully connect to the DB!")

	var programJogging Programs

	programSql := "SELECT id, name, created_on FROM programs WHERE name = $1"

	err = db.QueryRow(programSql, "Jogging Pagi").Scan(&programJogging.ID, &programJogging.Name, &programJogging.Created_on)
	if err != nil {
		log.Fatal("FAILED: failed to retrieve a program ", err.Error())
	}

	fmt.Printf("PROGRAM: [%d] %s | %s\n", programJogging.ID, programJogging.Name, programJogging.Created_on)
}
